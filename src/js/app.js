//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/masonry-layout/dist/masonry.pkgd.min.js
//= ../../node_modules/owl.carousel2/dist/owl.carousel.js
//= ../../node_modules/imagesloaded/imagesloaded.pkgd.min.js
//= ../../node_modules/jquery-validation/dist/jquery.validate.min.js

$(function() {

 	var openMenuClass = 'open-menu';
	var headerPanel = $('.header');
	var mainContainer = $('body');
	var btn = headerPanel.find('.mobile-menu-btn');

 	init();

	function init() {
		btnClickListener();
		initOwlCarousel();
		initMasonry();
	}	

	function btnClickListener() {
		if (btn.length) {
			btn.on('click', function () {
				mainContainer.toggleClass(openMenuClass);
			});
		}
	}

	function initOwlCarousel () {
		$('.full-carousel').owlCarousel({
			loop: true,
			nav: true,
            dotsEach: 7,
            autoplay : true,
			navText: "",
			items: 1,
		})

		$('.info-carousel').owlCarousel({
			loop: true,
			nav: true,
            autoplay : true,
			navText: "",
			items: 1,
		})

		$('.wedding-carousel').owlCarousel({
			loop: true,
			nav: true,
            autoplay : true,
			items: 1,
			navText: "",
			responsive:{

		        769:{
		        	margin: 25,
					center: true,
		            items: 2,
				}
		    }
		})
	}

	function initMasonry() {
		var $grid = $('.grid').masonry({
		  // options...
			itemSelector: '.grid-item',
			percentPosition: true,
			columnWidth: '.grid-sizer'
		});

		$grid.imagesLoaded().progress( function() {
		  $grid.masonry('layout');
		});	
	}

	function validateForm() {
		$(".form").validate({
			 rules: {
	            firstname: "required",
	            lastname: "required",
	            email: {
	                required: true,
	                email: true
	            },
	            phone: {
	            	required: true,
	            	number: true,
	            	rangelength: [7, 10]
	            }
	        },
	         messages: {
	            firstname: "Please enter your first name",
	            lastname: "Please enter your last name",
	            email: "Please enter a valid email address",
	            phone: {
	            	required: "Please enter a valid phone number",
	            	number: "Please enter a valid phone number",
	            	rangelength: "Phone must be from 7 to 9 numbers"
	        	}
	        },

	        submitHandler: function(form) {
	           var $form = $(form);
	            // let's select and cache all the fields
	            var $inputs = $form.find("input, select, button, textarea");
	            // serialize the data in the form
	            var serializedData = $form.serialize();

	            // let's disable the inputs for the duration of the ajax request
	            $inputs.prop("disabled", true);

	            // fire off the request to /form.php

	            request = $.ajax({
	                url: "send.php",
	                type: "post",
	                data: serializedData
	            });

	            // callback handler that will be called on success
	            request.done(function (response, textStatus, jqXHR) {
	                // log a message to the console
	                $('.form ').css('display','none');
	                $('.messages ').css('display','block');
	                
	            });

	            // callback handler that will be called on failure
	            request.fail(function (jqXHR, textStatus, errorThrown) {
	                // log the error to the console
	                console.error(
	                    "The following error occured: " + textStatus, errorThrown);
	            });

	            // callback handler that will be called regardless
	            // if the request failed or succeeded
	            request.always(function () {
	                // reenable the inputs
	                $inputs.prop("disabled", false);
	            });
	        }

		});
	}
	
});
